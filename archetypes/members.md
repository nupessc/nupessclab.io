---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
image: "images/members/{ .Name }.jpg"
description: "Cursando Ciências da Computação na Universidade Federal de Viçosa-Florestal."
lattes:
email:
linkedin:
github:
draft: false
---