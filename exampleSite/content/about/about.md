---
title: "Sobre"
date: 2019-10-29T13:49:23+06:00
draft: false

# image
$image: "images/author.jpg"

# meta description
description: "this is meta description"

# type
type : "about"
---

O Núcleo de Pesquisa em Engenharia de Software e Sistemas Colaborativos (NuPESSC) é um grupo de pesquisa do curso de Ciência da Computação da Universidade Federal de Viçosa - campus Florestal. Ele tem o intuito de possibilitar que alunos se engajem no universo da pesquisa científica em busca de soluções para problemas diversos da Engenharia de Software. Dentre as linhas de pesquisa endereçadas pelo NuPESSC, destacam-se:

-Aspectos colaborativos da Engenharia de Software;

-Medições sobre dados colaborativos para melhoria de processo de software;

-Integração semântica de informações de projetos com base em ontologias;

-Desafios da Engenharia de Software no universo Open Source;

-Testes baseados em modelos (Model Based Testing - MBT);

-Interdisciplinaridade no ensino de Engenharia de Software.