---
title: "Certificado SSL/TLS"
date: 2019-10-29T10:07:47+06:00
image: "images/tutorials/https-certificate.png"
draft: false
---
## Check current certificate

Access the [Qualys SSL Labs](https://www.ssllabs.com/ssltest/analyze.html?d=gitlab.nupessc.caf.ufv.br) for gitlab.nupessc.caf.ufv.br certificate check.


## Renew TLS certificate

Follow the steps:

1. Make sure apache2 service is stopped
    ```bash
    systemctl status apache2
    ```
    > Necessary due to port **80** assignment
1. Stop the service
    ```bash
    sudo systemctl stop apache2
    ```
1. Use certbot to renew certificate
    ```bash
    sudo certbot certonly --standalone -d nupessc.caf.ufv.br -d gitlab.nupessc.caf.ufv.br
    ```
1. Start the stopped apache2 service
    ```bash
    sudo systemctl start apache2
    ```