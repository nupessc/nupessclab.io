---
title: "Conectar à VPN (Windows)"
date: 2019-10-29T10:07:47+06:00
image: "images/tutorials/openvpn.jpg"
draft: false
---
## Get PuTTY

![imagem](/images/Tutorial-Windows/findPuTTY.png)

## Get OpenVPN Client

![imagem](/images/Tutorial-Windows/downloadOVPN.png)

## Download UFV VPN config

![imagem](/images/Tutorial-Windows/downloadOVPNconfig.png)

## Alternatively you can download a standalone VPN Client

![imagem](/images/Tutorial-Windows/AlternativelyWindowsInstall.png)

## Start OpenVPN Client

![imagem](/images/Tutorial-Windows/executeOVPN.png)

## Connect to VPN

![imagem](/images/Tutorial-Windows/runOnConfig.png)

![imagem](/images/Tutorial-Windows/typeCredentials.png)

![imagem](/images/Tutorial-Windows/startVPNConnection.png)

## Connect to server through SSH

![imagem](/images/Tutorial-Windows/connectSSHonPuTTY.png)


![imagem](/images/Tutorial-Windows/YouDidIt.png)
