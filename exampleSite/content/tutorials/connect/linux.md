---
title: "Conectar à VPN (Linux)"
date: 2019-10-29T10:07:47+06:00
image: "images/tutorials/openvpn.jpg"
draft: false
---
## Install OpenVPN

![imagem](/images/Tutorial-Linux/InstallOVPN.png)

## Download UFV VPN config

![imagem](/images/Tutorial-Linux/downloadConfig.png)

## Connect to VPN

![imagem](/images/Tutorial-Linux/connectOVPN.png)

## Connect to server through SSH

![imagem](/images/Tutorial-Linux/sshIntoServer.png)
