---
title: "Tomcat"
date: 2019-10-29T10:07:47+06:00
image: "images/tutorials/tomcat.jpg"
draft: false
---
## Check running containers

```
docker ps
```

Example:

|CONTAINER ID|IMAGE|COMMAND|CREATED|STATUS|PORTS|**NAMES**|
|---|---|:---:|---|---|:---:|---:|
|f35e6e776929|tomgatomod|`catalina.sh run`|14 months ago|Up 4 weeks|0.0.0.0:4002->8080/tcp|**tomgatodebotas**|
|b367138265a9|bdf004b053d0|`/sbin/tini -- /usr/local/bin/tomcat.sh`|15 months ago|Up 4 weeks|50000/tcp, 0.0.0.0:4001->5000/tcp, 0.0.0.0:4000->8080/tcp|**jenkins**|

> Use `-a` parameter to check the stopped containers and the running ones

## Check tomcat container stats

```
docker stats tomgatodebotas
```

Example:

|CONTAINER ID|NAME|CPU %|MEM USAGE / LIMIT|MEM %|NET I/O|BLOCK I/O|PIDS|
|---|---|---|---|---|---|---|---|
|f35e6e776929|tomgatodebotas|0.17%|693.1MiB / 5.689GiB|11.90%|39.7MB / 45.9MB|236MB / 0MB|62|

## Stop tomcat container

```
docker stop tomgatodebotas
```

## Resume stopped tomcat container

```
docker start tomgatodebotas
```

## Spawn a new tomcat container or start if it already exists

```
docker run -v /opt/tomcat/webapps/:/usr/local/tomcat/webapps/ --name tomgatodebotas -d -p 4002:8080 tomgatomod
```

## Run bash inside tomcat container

```
docker exec -ti tomgatodebotas bash
```

## Move file between tomcat container and host machine

From container to machine:
```
docker cp tomgatodebotas:/path/to/file /output/folder/path
```

From machine to container
```
docker cp /path/to/file tomgatodebotas:/output/folder/path
```

Example:
```
docker cp tomgatodebotas:/usr/local/tomcat/colminer.log .
```