---
title: "Jenkins"
date: 2019-10-29T10:07:47+06:00
image: "images/tutorials/jenkins.webp"
draft: false
---
## Check running containers

```
docker ps
```

Example:

|CONTAINER ID|IMAGE|COMMAND|CREATED|STATUS|PORTS|**NAMES**|
|---|---|:---:|---|---|:---:|---:|
|f35e6e776929|tomgatomod|`catalina.sh run`|14 months ago|Up 4 weeks|0.0.0.0:4002->8080/tcp|**jenkins**|
|b367138265a9|bdf004b053d0|`/sbin/tini -- /usr/local/bin/jenkins.sh`|15 months ago|Up 4 weeks|50000/tcp, 0.0.0.0:4001->5000/tcp, 0.0.0.0:4000->8080/tcp|**jenkins**|

> Use `-a` parameter to check the stopped containers and the running ones

## Check jenkins container stats

```
docker stats jenkins
```

Example:

|CONTAINER ID|NAME|CPU %|MEM USAGE / LIMIT|MEM %|NET I/O|BLOCK I/O|PIDS|
|---|---|---|---|---|---|---|---|
|b367138265a9|jenkins|0.05%|785.2MiB / 5.689GiB|13.48%|48.4MB / 2.35MB|274MB / 102MB|34|

## Stop jenkins container

```
docker stop jenkins
```

## Resume stopped jenkins container

```
docker start jenkins
```

## Spawn a new jenkins container or start if it already exists

```
docker run -v jenkins_var:/var --name jenkins -p 4000:8080 -p 4001:5000 -d primary157/jenkins
```

## Run bash inside jenkins container

```
docker exec -ti jenkins bash
```

## Move file between jenkins container and host machine

From container to machine:
```
docker cp jenkins:/path/to/file /output/folder/path
```

From machine to container
```
docker cp /path/to/file jenkins:/output/folder/path
```

Example:
```
docker cp jenkins:/var/jenkins_home/copy_reference_file.log .
```