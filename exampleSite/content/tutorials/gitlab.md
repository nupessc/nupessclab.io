---
title: "Gitlab"
date: 2019-10-29T10:07:47+06:00
image: "images/tutorials/gitlab.webp"
draft: false
---
## Stop Gitlab

```bash
    sudo gitlab-ctl stop
```
## Read config files and restart

```bash
    sudo gitlab-ctl reconfigure
```
## Restart Gitlab

```bash
    sudo gitlab-ctl restart
```
## Start Gitlab

```bash
    sudo gitlab-ctl start
```
## Check Gitlab status

```bash
    sudo gitlab-ctl status
```