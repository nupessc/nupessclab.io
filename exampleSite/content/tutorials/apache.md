---
title: "Apache"
date: 2019-10-29T10:07:47+06:00
image: "images/tutorials/apache.jpg"
draft: false
---
## Check apache2 status
    systemctl status apache2
## Stop apache2 service
    sudo systemctl stop apache2
## Start apache2 service
    sudo systemctl start apache2
## Restart apache2 modules and sites
    sudo apache2ctl restart
## Enable apache2 site
    sudo a2ensite <a file located in /etc/apache2/sites-available/>

> You should omit the `.conf` file extensions
## Disable apache2 site
    sudo a2dissite <a file located in /etc/apache2/sites-enabled/>

> You should omit the `.conf` file extensions
## Register another site config
1. Copy any file located in /etc/apache2/sites-available/
1. Change its values to fit your requirements
> You may be interested on checking this documentation: https://httpd.apache.org/docs/2.4/vhosts/examples.html