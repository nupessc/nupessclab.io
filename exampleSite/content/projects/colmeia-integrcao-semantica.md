---
title: "COLMEIA – Integração Semântica"
date: 2019-10-29T10:07:47+06:00
draft: false

team: [André Henrique Franco Costa]

# meta description
description: "Desenvolvimento de uma plataforma para extração e integração semântica, de forma semi-automatizada, de dados armazenados em repositórios de projetos de software. Os dados serão mapeados semanticamente para uma ontologia do domínio da Gerência de Configuração de Software, por meio de uma abordagem semi-automatizada que utiliza a base de dados de sinônimos do WordNet para relacionar tabelas a conceitos da ontologia, assim como campos de tabelas a propriedades de conceitos, a partir de correspondências entre os diversos significados semânticos dos mesmos. O mapeamento resultante poderá ser verificado e refinado, por meio de intervenção humana, antes que a carga dos dados seja realizada. Além disso, técnicas de aprendizado de máquina poderão ser adotadas com o intuito de melhorar o algoritmo de mapeamento semântico, que aprenderá a cada ajuste realizado. Para validar a abordagem proposta, serão realizados testes com dados de projetos de software armazenados no GitHub, com o intuito de se medir a acurácia da plataforma, quanto ao percentual de mapeamentos realizados, dados carregados e relações semânticas corretamente estabelecidas."

# post type
type: "projects"
---