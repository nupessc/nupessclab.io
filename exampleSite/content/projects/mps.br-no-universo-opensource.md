---
title: "MPS.BR no universo OpenSource"
date: 2019-10-29T10:07:47+06:00
draft: false

team: [Leonardo Júnio Alves dos Santos]

# meta description
description: "Este projeto objetiva aplicar os indicadores de qualidade do nível F do MPS.BR (Melhoria do Processo de Software Brasileiro), relacionados, inicialmente, ao processo de Gerência de Configuração de Software, com o intuito de analisar e classificar a maturidade de projetos de software Open Source armazenados no GitHub. Para isso, será desenvolvida uma ferramenta em Java, que utilizará a API GitHub para obter os dados dos projetos para então automatizar as medições com aplicação das métricas baseadas nos indicadores do MPS.BR."

# post type
type: "projects"

---