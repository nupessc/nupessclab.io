---
title: "Engenharia de Software para Desenvolvimento de FOSS"
date: 2019-10-29T10:07:47+06:00
draft: false

team: [Victor Guerra Veloso]

# meta description
description: "Desenvolvimento de uma metodologia baseada na produção de artefatos-chave em projetos com o intuito de melhorar a comunicação, atrair novos contribuintes, aumentar a motivação e o envolvimento dos desenvolvedores e reduzir problemas decorrentes de especificações inexistentes e interpretações equivocadas."

# post type
type: "projects"
---