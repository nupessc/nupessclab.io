---
title: "COLMEIA – Analisador de COLaborações baseado em MÉtrIcAs"
date: 2019-10-29T10:07:47+06:00
draft: false

team: [Cíntia Santos de Resende Silva Costa, Elena Augusta Araujo, Gabriel Gonçalves de Freitas, Santiago Gaudencio de Oliveira Souza]

# meta description
description: "Abordagem de análise do trabalho colaborativo da equipe em um projeto de software, denominada COLMEIA. Essa abordagem é baseada na aplicação de métricas quantitativas de avaliação do desempenho colaborativo dos membros de uma equipe, com o intuito de se obter indicadores que possam apoiar o processo de gerenciamento de tarefas do desenvolvimento."

# post type
type: "projects"
---