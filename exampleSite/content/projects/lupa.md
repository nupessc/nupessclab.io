---
title: "LUPA - Laboratório virtUal de Pesquisa e Aprendizagem"
date: 2019-10-29T10:07:47+06:00
draft: false

team: [Caio Augusto Moreira Campos, Cristian Amaral Silva]

# meta description
description: "O projeto LUPA abrange o desenvolvimento de um laboratório virtual de ensino que simula algumas práticas de conteúdos acadêmicos, proporcionando uma maior imersão dos alunos e reduzindo os custos com espaço físico, materiais e equipamentos usados em aulas práticas convencionais. Nesta primeira versão, o LUPA implementa um laboratório de biologia molecular com a simulação de um experimento para realização de um teste de paternidade. Para desenvolvimento do LUPA, são usadas boas práticas da Engenharia de Software, incluindo a modelagem UML das classes do sistema, e também engines gráficas e de modelagem 3D, além de recursos de gamificação para tornar o simulador mais atrativo ao público-alvo."

# post type
type: "projects"
---