---
title: "Testes de Sistemas Embarcados Baseados em MBT com UML-MARTE"
date: 2019-10-29T10:07:47+06:00
draft: false

team: [Tais Batista dos Santos]

# meta description
description: "Proposição de um método de testes para sistemas embarcados, que utiliza Model Based Testing (MBT), em uma abordagem caixa cinza, para gerar casos de teste a partir de modelos do sistema construídos em SysML e UML MARTE. Projetado para ser aplicado ainda na fase de design do sistema, esse método objetiva a identificação mais precoce de erros de design e a redução de custos do projeto. Para automatizar a aplicação do método proposto, será desenvolvida uma ferramenta capaz de receber como entrada diagramas construídos em SysML e/ou UML MARTE e, então, gerar e executar os casos de teste correspondentes. Inicialmente, será usado o Diagrama de Máquina de Estados, mas pretende-se adotar outros diagramas."

# post type
type: "projects"

---