---
title: "Representação Semântica do Conhecimento sobre Colaborações em um Processo de Software"
date: 2019-10-29T10:07:47+06:00
draft: false

team: [Aislan Rodrigo de Almeida Leite]

# meta description
description: "Desenvolvimento de representações semânticas do domínio das colaborações em um ambiente de desenvolvimento de software, visando extrair dados colaborativos, a partir do histórico de trabalho da equipe armazenado em repositórios de ferramentas da Gerência de Configuração de Software."

# post type
type: "projects"

---