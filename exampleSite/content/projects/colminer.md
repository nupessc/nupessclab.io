---
title: "ColMiner"
date: 2019-10-29T10:07:47+06:00
draft: false

image: "images/projects/colminer/logo.png"

team: [Luiz Eugênio Coelho Neto]

# meta description
description: "Desenvolver uma ferramenta para medir a relevância temática de comentários postados em ambientes de issue tracking."

# post type
type: "projects"
---

## Avalie a comunicação do seu projeto!

![1ª captura de imagem](/images/projects/colminer/1.png)
![2ª captura de imagem](/images/projects/colminer/2.png)
![3ª captura de imagem](/images/projects/colminer/3.png)


O ColMiner é uma ferramenta que visa apoiar o gerenciamento das comunicações, extraindo informações do ambiente de issue tracking do GitHub. Para isso, ele calcula a relevância temática dos comentários postados nos issues juntamente com algumas medidas quantitativas para ajudá-lo a identificar possíveis falhas de comunicação.

[Clique aqui e comece a utilizar!](http://tomcat.nupessc.caf.ufv.br/colminer/)

- Calcule o quão relevante é cada comentário para o tema da discussão do issue
- Crie gráficos com a relevância temática e/ou outras características dos issues e desenvolvedores.
- Utilize os gráficos para identificar quais desenvolvedores mais (ou menos) se comunicam;
- Investigue o perfil de cada desenvolvedor enquanto comunicador nos issues com os quais esteja envolvido.