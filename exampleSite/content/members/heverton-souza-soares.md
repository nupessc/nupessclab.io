---
title: "Heverton Souza Soares"
date: 2019-10-29T10:07:47+06:00
draft: false

# post thumb
image: "images/members/heverton-souza-soares.jpg"

# meta description
description: "Possui curso-tecnico-profissionalizante em Gestão de Processos Industriais pela SENAI - Departamento Regional de Minas Gerais(2012) e ensino-medio-segundo-graupela Escola Estadual Professor Cândido Azeredo(2009). Tem experiência na área de Ciência da Computação."

# post type
type: "members"

active: false

endDate: 2018-03-02

lattes:  http://lattes.cnpq.br/3444836622591647
email:
linkedin: https://www.linkedin.com/in/heverton-souza-soares-ba077b143/
github:
---