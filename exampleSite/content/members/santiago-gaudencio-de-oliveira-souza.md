---
title: "Santiago Gaudencio de Oliveira Souza"
date: 2019-10-29T10:07:47+06:00
draft: false

# post thumb
image: "images/members/santiago-gaudencio-de-oliveira-souza.jpg"

# meta description
description: "Graduado em Ciências da Computação pela Universidade Federal de Viçosa."

# post type
type: "members"

active: false

endDate: 2018-03-02

lattes:  http://lattes.cnpq.br/4001232757753503
email:
linkedin: https://www.linkedin.com/in/santiago-souza-aa943b151/
github:
---