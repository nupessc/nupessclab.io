---
title: "Tais Batista dos Santos"
date: 2019-10-29T10:07:47+06:00
draft: false

# post thumb
image: "images/members/tais-batista-dos-santos.jpg"

# meta description
description: "Atualmente é graduanda de Ciência da Computação pela Universidade Federal de Viçosa - Campus Florestal. É membro do Núcleo de Pesquisa em Engenharia de Software e Sistemas Colaborativos (NuPESSC). Possui interesse em usabilidade e testes automatizados."

# post type
type: "members"

active: false

endDate: 2018-03-02

lattes: http://lattes.cnpq.br/9940157701703227
email:
linkedin: https://www.linkedin.com/in/ta%C3%ADs-batista-23b69011b/
github:
---