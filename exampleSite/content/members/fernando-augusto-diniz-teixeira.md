---
title: "Fernando Augusto Diniz Teixeira"
date: 2019-10-29T10:07:47+06:00
draft: false

# post thumb
image: "images/members/fernando-augusto-diniz-teixeira.jpg"

# meta description
description: "Fernando Augusto Diniz Teixeira cursou Bacharelado em Ciência da Computação pela Universidade Federal de Viçosa. Possui como área de pesquisa: Testes de software/Automação de testes."

# post type
type: "members"

active: false

endDate: 2018-03-02

lattes:  http://lattes.cnpq.br/3482879848973804
email:
linkedin: 
github:
---