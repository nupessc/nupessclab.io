---
title: "Vitor Guerra Veloso"
date: 2019-12-31T15:20:42-03:00
draft: false

# post thumb
image: "images/members/victor-guerra-veloso.png"

# meta description
description: "Cursando Ciências da Computação na Universidade Federal de Viçosa-Florestal."

# post type
type: "members"

active: false

endDate: 2018-03-02

lattes: http://lattes.cnpq.br/1831616783896700
email: victorgvbh@gmail.com
linkedin: https://www.linkedin.com/in/victor-veloso-749b00188/
github: https://github.com/primary157/
---