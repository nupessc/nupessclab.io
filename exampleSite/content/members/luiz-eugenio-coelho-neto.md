---
title: "Luiz Eugênio Coelho Neto"
date: 2019-10-29T10:07:47+06:00
draft: false

# post thumb
image: "images/members/luiz-eugenio-coelho-neto.png"

# meta description
description: "Cursando Ciências da Computação na Universidade Federal de Viçosa-Florestal."

# post type
type: "members"

active: false

endDate: 2018-03-02

lattes: http://lattes.cnpq.br/9336171799583090
email:
linkedin: https://www.linkedin.com/in/luiz-eug%C3%AAnio-coelho-neto-4b8132150/
github:
---