---
title: "André Henrique Franco Costa"
date: 2019-12-31T15:20:42-03:00
draft: false

# post thumb
image: "images/members/andre-henrique-franco-costa.jpg"

# meta description
description: "Cursando Ciências da Computação na Universidade Federal de Viçosa-Florestal."

# post type
type: "members"

active: false

endDate: 2018-03-02

lattes: http://lattes.cnpq.br/2402494388767309
email:
linkedin:
github:
---