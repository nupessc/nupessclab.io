---
title: "João Teixeira Araújo"
date: 2019-10-29T10:07:47+06:00
draft: false

# post thumb
image: "images/members/joao-teixeira-araujo.jpg"

# meta description
description: "Possui graduação em Ciência da Computação, na área de desenvolvimento de sistemas colaborativos web, implementados em Java. Atual mestrando no programa de pós-graduação da Universidade Federal de São João del-Rei (UFSJ), com o projeto entitulado \"Uso de Descritores de áudio para análise de timbre\"."

# post type
type: "members"

active: false

endDate: 2018-03-02

lattes:  http://lattes.cnpq.br/3516282920252296
email:
linkedin: 
github:
---