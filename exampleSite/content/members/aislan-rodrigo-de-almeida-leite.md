---
title: "Aislan Rodrigo de Almeida Leite"
date: 2019-10-29T10:07:47+06:00
draft: false

# post thumb
image: "images/members/aislan-rodrigo-de-almeida-leite.jpg"

# meta description
description: "Possui graduação em Ciência da Computação pela Universidade Federal de Viçosa(2017) e ensino-medio-segundo-graupela SESI - Departamento Regional de Minas Gerais(2007). Tem experiência na área de Ciência da Computação, com ênfase em Metodologia e Técnicas da Computação."

# post type
type: "members"

active: false

endDate: 2018-03-02

lattes:  http://lattes.cnpq.br/4909618719592329
email:
linkedin: https://www.linkedin.com/in/aislan-leite/
github:
---