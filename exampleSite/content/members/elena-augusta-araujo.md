---
title: "Elena Augusta Araujo"
date: 2019-10-29T10:07:47+06:00
draft: false

# post thumb
image: "images/members/elena-augusta-araujo.jpg"

# meta description
description: "Mestre em Ciência da Computação no Programa de Pós Graduação da Universidade Federal de Lavras - UFLA em 2019. Bacharela em Ciência da Computação pela Universidade Federal de Viçosa - Campus Florestal em Janeiro 2017. Tem experiência na área de Ciência da Computação, com ênfase em Engenharia de Software, Conformidade Arquitetural e Arquitetura de Microsserviços."

# post type
type: "members"

active: false

endDate: 2018-03-02

lattes:  http://lattes.cnpq.br/2284224784688900
email:
linkedin: 
github:
---