---
title: "Cíntia Santos de Resende Silva Costa"
date: 2019-10-29T10:07:47+06:00
draft: false

# post thumb
image: "images/members/cintia-santos-de-resende-silva-costa.jpg"

# meta description
description: "Cursando Ciências da Computação na Universidade Federal de Viçosa-Florestal."

# post type
type: "members"

active: false

endDate: 2018-03-02

lattes: http://lattes.cnpq.br/3731224547467681
email:
linkedin: https://www.linkedin.com/in/c%C3%ADntia-costa-098b0480/
github:
---