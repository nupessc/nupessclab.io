---
title: "Caio Augusto Moreira Campos"
date: 2019-10-29T10:07:47+06:00
draft: false

# post thumb
image: "images/members/caio-augusto-moreira-campos.jpg"

# meta description
description: "Cursando Ciências da Computação na Universidade Federal de Viçosa-Florestal."

# post type
type: "members"

active: true

endDate: 2018-03-02

lattes: http://lattes.cnpq.br/5333419151696312
email:
linkedin: https://www.linkedin.com/in/caio-augusto-moreira-campos-972524150/
github:
---