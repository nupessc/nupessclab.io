---
title: "Gabriel Gonçalves de Freitas"
date: 2019-10-29T10:07:47+06:00
draft: false

# post thumb
image: "images/members/gabriel-gonçalves-de-freitas.jpg"

# meta description
description: "Possui curso-tecnico-profissionalizante em Técnico em Informática pela Universidade Federal de Viçosa(2013), ensino-fundamental-primeiro-grau pela ESCOLA ESTADUAL SERAFIM RIBEIRO DE REZENDE(2010) e ensino-medio-segundo-graupela Universidade Federal de Viçosa(2013)."

# post type
type: "members"

active: false

endDate: 2018-03-02

lattes:  http://lattes.cnpq.br/5592170155056786
email:
linkedin: https://www.linkedin.com/in/gabrielgonalves/
github:
---