---
title: "Vítor Luís Gonçalves Silva"
date: 2019-12-31T15:20:42-03:00
draft: false

# post thumb
image: "images/members/vitor-luis-goncalves-silva.jpg"

# meta description
description: "Cursando Ciências da Computação na Universidade Federal de Viçosa-Florestal."

# post type
type: "members"

active: false

endDate: 2018-03-02

lattes: http://lattes.cnpq.br/2808512210326057
email:
linkedin: https://www.linkedin.com/in/v%C3%ADtor-lu%C3%ADs-7b1448131/
github: https://github.com/vitor-l
---