---
title: "Glaucia Braga E Silva"
date: 2019-12-31T15:20:42-03:00
draft: false

# post thumb
image: "images/members/glaucia-braga-e-silva.jpg"

# meta description
description: "Possui Graduação em Ciência da Computação pela Universidade Federal de Lavras - UFLA, com Mestrado e Doutorado em Computação pelo Instituto Tecnológico de Aeronáutica - ITA. Atualmente é professora do Instituto de Ciências Exatas e Tecnológicas da Universidade Federal de Viçosa - Campus Florestal. Tem experiência e interesse nas seguintes sub-áreas da Computação: Tecnologias Semânticas, Ambientes Colaborativos, Modelagem de Sistemas e Processos de Software."

# post type
type: "members"

active: true

endDate: 2018-03-02

lattes: http://lattes.cnpq.br/2427091227765800
email:
linkedin: https://www.linkedin.com/in/gl%C3%A1ucia-braga-e-silva-a0b0a7ba/
github:
---