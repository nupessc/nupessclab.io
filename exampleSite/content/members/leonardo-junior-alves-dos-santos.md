---
title: "Leonardo Júnio Alves dos Santos"
date: 2019-10-29T10:07:47+06:00
draft: false

# post thumb
image: "images/members/leonardo-junior-alves-dos-santos.png"

# meta description
description: "Cursando Ciências da Computação na Universidade Federal de Viçosa-Florestal."

# post type
type: "members"

active: false

endDate: 2018-03-02

lattes: http://lattes.cnpq.br/6082003509071614
email:
linkedin:
github:
---